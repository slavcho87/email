package com.adidas.email.email.controller;

import com.adidas.email.subscriber.domain.Subscriber;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class EmailController {

    @KafkaListener(topics = "sendEmail")
    public void sendEmail(Subscriber subscriber){
        System.out.println("-------------------------------------------");
        System.out.println("TO: "+subscriber.getEmail());
        System.out.println(subscriber.getFirstName()+" thanks for subscribe :)");
        System.out.println("-------------------------------------------");
    }

}
