# Adidas Code Challenge - Email

## Environment installation

A prerequisite is that you have docker previously installed. 

For the installation of this project a script has been provided in another repository (URL). Anyway we leave the necessary commands for building and booting the docker images.

# Build
To build the docker image run the following command:

    docker build -t adidas/email .
    
# Run
To run the docker image run the following command:

    docker run --network kafka_default adidas/email -d

# Use

This project is a mock that simulates the sending of emails. Once the docker image is started, the data received from the apache kafka queue is left in the console:

    -------------------------------------------
    TO: mario@test.com
    Mario thanks for subscribe :)
    -------------------------------------------

# Technologies used

In this project the following technologies have been used:

- Java 1.8
- spring boot v2.4.0
- lombok v1.18.24: library that automatically plugs into your editor and build tools, spicing up your java.
- spring-kafka v2.8.6: apache kafka consumer
