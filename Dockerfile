FROM maven:3-jdk-8

VOLUME /tmp
COPY /target/email.jar email.jar
EXPOSE 8081
ENTRYPOINT ["java", "-jar", "/email.jar"]
